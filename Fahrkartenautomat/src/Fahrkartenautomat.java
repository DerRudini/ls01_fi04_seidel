﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double rückgabebetrag;
       double eingezahlterGesamtbetrag;

       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       rueckgeldAusgeben(rückgabebetrag);
       
    }
    
    public static double fahrkartenbestellungErfassen(Scanner tastatur) {
    	
    	System.out.print("Zu zahlender Betrag (EURO): ");
    	double zuZahlenderBetragTemp = tastatur.nextDouble();
    	System.out.println("Wählen Sie eins der 3 Tickets aus und geben Sie die entsprechende Zahl an:");
    	System.out.println("Ticket 1");
    	System.out.println("Ticket 2");
    	System.out.println("Ticket 3");
    	int ticketAuswahl = tastatur.nextInt();
    	if (ticketAuswahl == 1) {
    		zuZahlenderBetragTemp = 1;
    	}
    	else if (ticketAuswahl == 2) {
    		zuZahlenderBetragTemp = 2;
    	}
    	else if (ticketAuswahl == 3) {
    		zuZahlenderBetragTemp = 3;
    	}
    	System.out.print("Anzahl Tickets: ");
    	int ticketAnzahl = tastatur.nextInt();
        if(ticketAnzahl < 0 || ticketAnzahl > 10) {
            System.out.println("Die angegebene Anzahl ist ungültig! Fahre mit 1 fort...");
            ticketAnzahl = 1;
        }
        if (zuZahlenderBetragTemp < 0) {
        	System.out.print("Ungultiger Betrag. Fahre mit 1 fort ...");
        	zuZahlenderBetragTemp = 1;
        }

        return zuZahlenderBetragTemp * ticketAnzahl; 

    }

    

    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.println("Noch zu zahlen: " + String.format("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag)));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    } 
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f", rückgabebetrag) + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
        main(null);
    }
}