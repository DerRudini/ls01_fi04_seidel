package Temperatur;

public class Temperatur {
	public static void main(String[] args) {
		System.out.printf("%-12s", "Fahrenheit");
		System.out.printf("|");
		System.out.printf("%10s", "Celsius");
		System.out.printf("\n");
		
		System.out.printf("-----------------------");
		System.out.printf("\n");
		
		System.out.printf("%-12s", "-20");
		System.out.printf("|");
		System.out.printf("%10s", "28.8889");
		System.out.printf("\n");
		
		System.out.printf("%-12s", "-10");
		System.out.printf("|");
		System.out.printf("%10s", "-23.3333");
		System.out.printf("\n");
		
		System.out.printf("%-12s", "0");
		System.out.printf("|");
		System.out.printf("%10s", "-17.7778");
		System.out.printf("\n");
		
		System.out.printf("%-12s", "20");
		System.out.printf("|");
		System.out.printf("%10s", "-6.6667");
		System.out.printf("\n");
		
		System.out.printf("%-12s", "30");
		System.out.printf("|");
		System.out.printf("%10s", "-1.1111");
		System.out.printf("\n");


	}
}
